function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%

% Suggested values for C and sigma.
% The double loop generates all pairs of these
Cs = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30];
sigmas = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30];

% We want to keep track of the smallest error, since that
% indicates we want to keep a C and sigma for that.
minimumError = intmax;

for c = Cs
    for s = sigmas
        % Train the model; the anonymous function %(x1, x2)... trains on
        % every pair of xi xj from X.
        model = svmTrain(X, y, c,@(x1, x2) gaussianKernel(x1, x2, s));

        % Use the validation set with the model we just trained.
        predictions = svmPredict(model, Xval);

        %Compute the predicted error.
        predError = mean(double(predictions ~= yval));

        % Have we done better than in the past?
        if predError < minimumError
            minimumError = predError;
            C = c;
            sigma = s;
        endif
    endfor
endfor

% =========================================================================

end
