function J = logisticCost(h, y)

%cost function only. 
% h is the h_theta(x) -- sigmoid applied to all entries
% y is the results column

m = length(y);

J = (1/m) * sum((-y .* log(h)) - ((1 - y) .* log(1 - h)));

end