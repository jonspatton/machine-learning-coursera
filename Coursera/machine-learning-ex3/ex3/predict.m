function p = predict(Theta1, Theta2, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(Theta2, 1);

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned neural network. You should set p to a 
%               vector containing labels between 1 to num_labels.
%
% Hint: The max function might come in useful. In particular, the max
%       function can also return the index of the max element, for more
%       information see 'help max'. If your examples are in rows, then, you
%       can use max(A, [], 2) to obtain the max for each row.
%

% Add the bias column to X
% Each X should be a column vector, so we could flip X on its side,
% or we can do less transposing and flip the Thetas over.
a1 = [ones(m, 1) X];
% size(a1) % is examples x features + 1

% Theta1 is p x features, so this comes out as m x p + 1 (after adding bias)
z2 = (a1 * Theta1');
a2 = [ones(m, 1) sigmoid(z2)];

% size(a2) % is examples x theta1 features + 1

% We only need to apply the activation function to the results of layer 2
% Theta2 has the same number of columns as the final features
% so this comes out as m x k
z3 = a2 * Theta2';
a3 = (sigmoid(z3));

% As before, return a matrix p containing the indices of the maximum elements in
% each column.
[_, p] = max(a3, [], 2);

% =========================================================================

end
