function [e_trains e_vals ...
            trainingAverage validationAverage ...
            e_trains_avg e_vals_avg] = ...
            trainingCurvesAverages(X, y, Xval, yval, examples, lambda)

% Randomizes samples and returns the data necessary to plot learning
% curves based on a large number of random samples of each size.

% Inputs:
% A training set, X, and its outputs, y.
% A validation set, Xval, and its outputs, yval.
% The number of examples to gather (how many times to loop over
% randomized sets).
% The lambda value to use when training Theta with each randomized set.

% Outputs:
% e_trains is a matrix of each randomized training example error values.
% e_vals is a vector of each randomized validation example error values.
% trainingAverage is the value averaged over every randomized training sets
% and all examples.
% validationAverage is the same for the validation set.
% e_trains_avg is a vector with the average for each set size,
% after performing all the example loops.
% e_vals_avg is the same for the validation set.
 
largest_sample_size = min(size(X)(1), size(Xval)(1))
e_trains = zeros(examples, largest_sample_size);
e_vals = zeros(examples, largest_sample_size);

for i = 1:examples
    for j = 1:largest_sample_size
        %build random sample from X
        indices_val = randperm(largest_sample_size, j);
        indices_X = randperm(largest_sample_size, j);
        X_rand = X(indices_X(1), :);
        y_rand = y(indices_X(1), :);
        X_rand_val = Xval(indices_val(1), :);
        y_rand_val = yval(indices_val(1), :);
        for k = 2:j
            X_rand = [X_rand; X(k, :)];
            y_rand = [y_rand; y(k, :)];
            X_rand_val = [X_rand_val; Xval(k, :)];
            y_rand_val = [y_rand_val; yval(k, :)];
        endfor
        % calculate errors and save them.
        [theta] = trainLinearReg(X_rand, y_rand, lambda);
        e_trains(i, j) = linearRegCostFunction(X_rand, y_rand, theta, 0);
        e_vals(i, j) = linearRegCostFunction(X_rand_val, y_rand_val, theta, 0);
    endfor
endfor

trainingAverage = sum(sum(e_trains))/(examples * largest_sample_size);
validationAverage = sum(sum(e_vals))/(examples * largest_sample_size);
e_trains_avg = sum(e_trains)/examples;
e_vals_avg = sum(e_vals)/examples;