function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%

% Cost (J)

%Hypothesis matrix -- for this function, the sum of linear terms x_1 + theta_1 x_2 + ...
h = X * theta;

% The sum of squares term
S = (1 / (2 * m)) * sumOfSquares(h, y);

% the regularization term
L_c = (lambda / (2 * m)) * sum(theta(2:end, :) .^ 2);

% Cost fuction.
J = S + L_c;

% Gradients

%The (h - y)x term
H = (1 / (m)) * (X' * (h - y));

% Regularization term
% calculate the penalty for the thetas, but do not penalize theta_0
T = [0; theta(2:end, :)];
L_g = (lambda/m) * (T);

grad = H + L_g;

% =========================================================================

grad = grad(:);
end
