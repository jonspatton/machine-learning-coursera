function S = sumOfSquares(X, y)

% Computes the sum of squares of a the entries of matrix X and vector y.

X = (X - y);

S = sum(X' * X );
