function centroids = computeCentroids(X, idx, K)
%COMPUTECENTROIDS returns the new centroids by computing the means of the 
%data points assigned to each centroid.
%   centroids = COMPUTECENTROIDS(X, idx, K) returns the new centroids by 
%   computing the means of the data points assigned to each centroid. It is
%   given a dataset X where each row is a single data point, a vector
%   idx of centroid assignments (i.e. each entry in range [1..K]) for each
%   example, and K, the number of centroids. You should return a matrix
%   centroids, where each row of centroids is the mean of the data points
%   assigned to it.
%

% Useful variables
[m n] = size(X);

% You need to return the following variables correctly.
centroids = zeros(K, n);


% ====================== YOUR CODE HERE ======================
% Instructions: Go over every centroid and compute mean of all points that
%               belong to it. Concretely, the row vector centroids(i, :)
%               should contain the mean of the data points assigned to
%               centroid i.
%
% Note: You can use a for-loop over the centroids to compute this.
%

% Holds the total number of examples close to centroid 1...k for this dataset
C_k = zeros(K, 1);

for i = 1:m
    index = idx(i);
    %Increment the C_k for this example
    C_k(index) = C_k(index) + 1;

    centroids(index, :) = centroids(index, :) + X(i, :);
endfor

% We now have a set of centroids that are the sum of all their assigned examples
% and a vector C_k that has the total number of examples assigned.
% So for each centroid, divide it by the number of examples assigned.

centroids = centroids ./ C_k;

% =============================================================

end

