function J = computeCost(X, y, theta)
%COMPUTECOST Compute cost for linear regression
%   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
%   parameter for linear regression to fit the data points in X and y

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
%J = 0;

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta
%               You should set J to the cost.

% Theta is the vector parameter of the function 
% J(theta) := 1/2m * sum from 1 to m of ((x(i) - y(i))^2).
% X is the data points matrix containing each x_j^(i), so
% X * theta is an m-dimentional vector. Since y contains the
% right hand sides, we can subtract the two vectors and then
% component-wise square their values, multiply times the scalar,
% and we are done. 

J = 1/(2*m) * sum((X * theta - y).^2);

% =========================================================================

end
