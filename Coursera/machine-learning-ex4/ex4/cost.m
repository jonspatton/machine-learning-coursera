function h = cost(h, y)

%cost function only. 
% h is the h_theta(x) -- sigmoid applied to all entries
% y is the results column(s)

m = length(y);

h = (-y .* log(h)) - ((1 - y) .* log(1 - h));

end