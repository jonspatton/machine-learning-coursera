function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
n = size(X, 2);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m

% y(i) is a member of Y = {[1, ..., 0], [0, 1, ..., 0], ... , [0, ..., 1]}
% So we need a matrix with K columns, and each column has a 1 in the column
% indicated by the value given in y
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.

Y = zeros(m, num_labels);
for i = [1:m]
    Y(i, y(i)) = 1;
endfor;

% add a bias column
a1 = [ones(m, 1) X];

% First layer
z2 = a1 * Theta1';

% Add a bias column
a2 = [ones(size(z2, 1), 1) sigmoid(z2)];

%Hidden layer output.
z3 = a2 * Theta2';

%Output layer
a3 = sigmoid(z3); 

% For the lambda term, we need the triple sum described in class.
% These are the sums of the square of every entry except the first column of the Thetas;
% We then sum the two thetas.
T1_squares = sum(sum(Theta1(:, 2:end).^2, 2));
T2_squares = sum(sum(Theta2(:, 2:end).^2, 2));
thetaSquares = T1_squares + T2_squares;

J = (1/m) * sum(sum(cost(a3, Y))) + (lambda / (2*m)) * thetaSquares;

% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients

% Compute the deltas for each layer.
delta_3 = a3 - Y;
delta_2 = (delta_3 * Theta2) .* sigmoidGradient([ones(size(z2, 1), 1) z2]);

% Need to remove the bias column
delta_2 = delta_2(:, 2:end);

% Multiply current layer delta by previous layer weighted values.
DELTA_1 = delta_2' * a1;
DELTA_2 = delta_3' * a2;

% divide the grads by m
Theta1_grad = (1/m) * DELTA_1;
Theta2_grad = (1/m) * DELTA_2;

% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

regularization1 = (lambda/m)*Theta1;
regularization2 = (lambda/m)*Theta2;

% "Note that you should not be regularizing the first column of Θ(l) which
% is used for the bias term."
% (Replace the first columns with zeros)

regularization1(:, 1) = zeros(1, size(Theta1));
regularization2(:, 1) = zeros(1, size(Theta2));


Theta1_grad = Theta1_grad + regularization1;
Theta2_grad = Theta2_grad + regularization2;


% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
