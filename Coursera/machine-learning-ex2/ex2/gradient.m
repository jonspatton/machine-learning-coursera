function grad = gradient(h, y, X)

% computes the gradient (first derivatives) of a matrix h given training results y
% and X is the training examples

m = length(y);

grad = (1/m) * X' * (h - y);

end