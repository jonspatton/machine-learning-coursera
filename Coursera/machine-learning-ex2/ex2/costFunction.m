function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%

% Matrix containing each h_theta(x) as rows
% The course definition is theta' * x, but if you want to do the whole
% thing at once, you have to do X*Theta, where X is the matrix of xs.

h = sigmoid(X*theta); %'run h_theta (X);

% Cost function
% J = (1/m) * sum((-y .* log(h)) - ((1 - y) .* log(1 - h)));
J = logisticCost(h, y);

% partial derivatives for each column
% grad = (1/m) * sum((h .- y) .* X);

grad = gradient(h, y, X);

% =============================================================

end
