# Implementation from Neural Networks: A Visual Introduction for Beginners
# by Michael Taylor

# This contains adjustments and/or experimentations with the code.
# This version of the network is >95% accuracy.

# The book was okay but his implementation was not great.
# With the default values, it only achieves 92% accuracy,
# but 10 epochs take the running time of ~40 with this version.

# TODO (if I mess with this version more):
# Add save function;
# Empirically determine best learning rate automatically.
# Empyrically determine best number of epochs
# Refactor to an object-based approach so I can
# memoirize multiple trained models for testing.

# Might be better to just do some other tutorials.

import tensorflow as tf
import keras
import matplotlib.pyplot as plt
import numpy as np

#######################
# Our dataset
#######################
from tensorflow.examples.tutorials.mnist import input_data

# If we change datasets, make sure that the input size and
# output nodes are also adjusted.
mnist = input_data.read_data_sets("MNIST_data", source_url='http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/', one_hot = True)
#mnist = input_data.read_data_sets("MNIST_data", one_hot = True)

# Plotting code from https://github.com/zalandoresearch/fashion-mnist
label_dict = {0:0, 1:1, 2:2, 3:3, 4:4, 5:5, 6:6, 7:7, 8:9}
# Get 28x28 image
sample_1 = mnist.train.images[0].reshape(28,28)
# Get corresponding integer label from one-hot encoded data
sample_label_1 = np.where(mnist.train.labels[47] == 1)[0][0]
# Plot sample
print("y = {label_index} ({label})".format(label_index=sample_label_1, label=label_dict[sample_label_1]))
plt.imshow(sample_1, cmap='Greys')

plt.show()

###########################
# TensorFlow graph creation
###########################

# alpha rate for the network
learning_rate = 0.25
# Original value is very small (0.0001), and thus slow;
# fewer layers, higher rate actually works better and faster on the
# original set.
# The best for the mnist set with 100 hidden nodes is ~.5 or .6 for this build.
# The fashion set needs more experimentation but extra layers and such
# still don't actually help, so some other work would need to be done.

# We'll train in batches rather than the full dataset at once.
batch_size = 100
# Batch size of 100 is fine for the digits set. The fashion set
# probably needs to be a bit smaller.

# And update every so often to the user
update_step = 10

# Hidden layers
# The pictures in the mnist dataset are 28x28
input_nodes = 28 * 28
layer_1_nodes = 150     #100 is fine for the digits set.
#layer_2_nodes = 500     # Honestly I think these extra layers are overkill
#layer_3_nodes = 500

# Output -- 10 digits, 0-9
output_nodes = 10


#######################
# Network model
#######################

# These are placeholders, empty tensors that will be fed data 
# as the network operates.
network_input = tf.placeholder(tf.float32, [None, input_nodes])
target_output = tf.placeholder(tf.float32, [None, output_nodes])

# This is the function we will use for selecting random weights.
# (TensorFlow has others, so we might want to change it.)
rand_f = tf.random_normal

# Initialize the node weights to random values from a normal sample set.
def initialize_rand(f, t, rand_f):
    '''
    Return a tuple with the weight matrix (sized from x to) initialized
    with random weights and the layer bias vector (usually a bunch of 1s).
    '''
    return tf.Variable(rand_f([f, t])), tf.Variable(rand_f([t]))

layer_1, layer_1_bias = initialize_rand(input_nodes, layer_1_nodes, rand_f)
#layer_2, layer_2_bias = initialize_rand(layer_1_nodes, layer_2_nodes, rand_f)
#layer_3, layer_3_bias = initialize_rand(layer_2_nodes, layer_3_nodes, rand_f)
output_layer, output_layer_bias = initialize_rand(layer_1_nodes, output_nodes, rand_f)

#layer_1 = tf.Variable(tf.random_normal([input_nodes, layer_1_nodes]))
#layer_1_bias = tf.Variable(tf.random_normal([layer_1_nodes]))

#layer_2 = tf.Variable(tf.random_normal([layer_1_nodes, layer_2_nodes]))
#layer_2_bias = tf.Variable(tf.random_normal([layer_2_nodes]))

#layer_3 = tf.Variable(tf.random_normal([layer_2_nodes, layer_3_nodes]))
#layer_3_bias = tf.Variable(tf.random_normal([layer_3_nodes]))

#output_layer = tf.Variable(tf.random_normal([layer_1_nodes, output_nodes]))
#output_layer_bias = tf.Variable(tf.random_normal([output_nodes]))

##########################
# Feedforward calculations
##########################

# Our activation function. TF has others, so we might want to try
# something else.
activation = tf.nn.relu

def calcOutput(f, t, b, activation):
    '''
    Caculate the output of a layer by multiplying from x to and 
    adding the bias
    '''
    if activation:
        return activation(tf.matmul(f, t) + b)
    return tf.matmul(f, t) + b

l1_output = calcOutput(network_input, layer_1, layer_1_bias, activation)
#l2_output = calcOutput(l1_output, layer_2, layer_2_bias, activation)
#l3_output = calcOutput(l2_output, layer_3, layer_3_bias, activation)

# Don't apply any activation function to the raw output.
ntwk_output_1 = calcOutput(l1_output, output_layer, output_layer_bias, None)

#l1_output = tf.nn.relu(tf.matmul(network_input, layer_1) + layer_1_bias)
#l2_output = tf.nn.relu(tf.matmul(l1_output, layer_2) + layer_2_bias)
#l3_output = tf.nn.relu(tf.matmul(l2_output, layer_3) + layer_3_bias)

#ntwk_output_1 = tf.matmul(l3_output, output_layer) + output_layer_bias

# Our activation function specifically used for classification.
# Softmax is the exponential function on the tensor's members,
# divided by the sum along the dimension it's performed on.
# https://github.com/tensorflow/tensorflow/blob/r1.12/tensorflow/python/ops/nn_ops.py
# TensorFlow has others, so we might want to change this.
classification = tf.nn.softmax
# 
ntwk_output_2 = tf.nn.softmax(ntwk_output_1)

#l1_output = activation(tf.matmul(network_input, layer_1) + layer_1_bias)
#l2_output = activation(tf.matmul(l1_output, layer_2) + layer_2_bias)
#l3_output = activation(tf.matmul(l2_output, layer_3) + layer_3_bias)

##########################
# Define training elements
##########################

# Cost function.
cf = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
    logits = ntwk_output_1, labels = target_output))

# Size of the training step taken toward minimizing the cost function
ts = tf.train.GradientDescentOptimizer(learning_rate).minimize(cf)

# Performance function -- compare the network output with the true label.
# The output is expressed in True/False values.
cp = tf.equal(tf.argmax(ntwk_output_2, 1), tf.argmax(target_output, 1))

# Average number of correct predictions. This converts the t/f in cp to floats.
acc = tf.reduce_mean(tf.cast(cp, tf.float32))

##########################
# Do the thing!
##########################

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    num_epochs = 40 # Worth experimenting with.

    for epoch in range(num_epochs):
        total_cost = 0
        for _ in range (int(mnist.train.num_examples / batch_size)):
            batch_x, batch_y = mnist.train.next_batch(batch_size)
            feed_dict = {network_input : batch_x,
                        target_output : batch_y}
            t, c = sess.run([ts, cf], feed_dict = feed_dict)

            total_cost += c

        print("Epoch", epoch + 1, "of", num_epochs, ". Loss:", total_cost)

    print("Accuracy:", acc.eval({network_input : mnist.test.images,
                                 target_output : mnist.test.labels}))   