from NeuralNetwork import *
from numpy import asfarray, array, amax
import matplotlib.pyplot as plt

def main():

    # Create the training set.
    # Images are 28x28 pixels
    training_set = separated_file_to_list("train_images.csv")
    training_labels = separated_file_to_list("train_labels.csv")

    test_set = separated_file_to_list("test_images.csv")
    test_labels = separated_file_to_list("test_labels.csv")

    # Image test
    #h = 28
    #w = 28
    #image_array = np.asfarray(training_set[0]).reshape((h, w))
    #plt.imshow(image_array, cmap = 'Greys', interpolation = 'None')
    #plt.show()
    #pause()

    rates = {0.08 : None, 0.1 : None, 0.12 : None, .14 : None,
             .16 : None, .18 : None, .2 : None, .22 : None, 
             .24 : None, .26 : None, .28 : None, .3 : None}
    best_rate = 0
    best_accuracy = 0

    for rate in rates.keys():
        #input, hidden, output nodes, and learning rate
        in_nodes = len(training_set[0])
        hidden_nodes = 100
        out_nodes = 10
        r = rate

        n = NeuralNetwork(in_nodes, hidden_nodes, out_nodes, r)

        scaled_training_input = normalizeValues(training_set)
        scaled_test_input = normalizeValues(test_set)
    
        #Train the network on all training data.
        n.train_network(scaled_training_input, array(training_labels))

        #test the neural network
        n.test(scaled_test_input, array(test_labels))

        scorecard = n.scorecard

        accuracy = float(sum(scorecard))/len(scorecard)

        print("\n\naccuracy: ", accuracy, "with learning rate ", rate)

        rates[rate] = accuracy

        if accuracy > best_accuracy:
            best_accuracy = accuracy
            best_rate = rate
    print("best rate and accuracy: ", best_rate, best_accuracy)
    print("All rates and accuracies:")
    print(rates)

    #Plot the learning rates
    names = list(rates.keys())
    values = list(rates.values())

    fig, ax = plt.subplots()
    ax.plot(names, values, label = 'rate')
    fig.suptitle('Accuracy results with different learning rates')
    plt.show()
    pause()

def separated_file_to_list(filename, separator = ','):
    '''
        Takes a separated file (e.g. csv, tab-separated, etc.)
        Reads line by line and returns a list of the read values
    '''
    l = []
    with open(filename) as f:
        for line in f:
            length = len(line)
            l.append([int(x) for x in line[:length - 1].split(',')])
    return l

def normalizeValues(values_list, mult_epsilon = 0.99, add_epsilon = 0.01):
    '''
    Normalize a set of values. The values are assumed to be positive for this function.
    mult_epsilon is the factor by which to multiply elements after normalizing
    add_epsilon is the small value to add.
    These values prevent the normalized value from being 1 or 0.
    '''
    return (asfarray(values_list) / float(amax(values_list)) * mult_epsilon) + add_epsilon

def pause():
    #################################################################################
    ''' Request arbitrary user input before continuing.'''
    #################################################################################
    input("Press ENTER to continue.")

main()