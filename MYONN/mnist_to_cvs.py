from mlxtend.data import loadlocal_mnist
import numpy as np

########################################################################
#Converts the mnist data to CSV files.
# from http://rasbt.github.io/mlxtend/user_guide/data/loadlocal_mnist/
# mlextend from https://github.com/rasbt/mlxtend
########################################################################

X, y = loadlocal_mnist(
        images_path = 'train_set/train-images-idx3-ubyte',
        labels_path = 'train_set/train-labels-idx1-ubyte'
    )

np.savetxt(fname = 'train_images.csv', X = X, delimiter = ',', fmt = '%d')
np.savetxt(fname = 'train_labels.csv', X = y, delimiter = ',', fmt = '%d')

X, y = loadlocal_mnist(
    images_path = 'test_set/t10k-images-idx3-ubyte',
    labels_path = 'test_set/t10k-labels-idx1-ubyte'
    )

np.savetxt(fname = 'test_images.csv', X = X, delimiter = ',', fmt = '%d')
np.savetxt(fname = 'test_labels.csv', X = y, delimiter = ',', fmt = '%d')