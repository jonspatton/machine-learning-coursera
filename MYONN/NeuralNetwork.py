#Implementation from Tariq Rashid's Make Your Own Neural Network

from numpy import array, random, zeros, dot, asfarray, argmax, transpose
from scipy.special import expit as sigmoid

class NeuralNetwork:

    # Attributes
    # learning_rate, the constant multiplier during gradient descent
    # in_nodes, number of inputs
    # hidden_nodes, number of hidden nodes
    # wih, the matrix of weights from the input to hidden layer
    # who, the matrix of weights from the hidden layer to output
    # activation function, a function such as the sigmiod

    def __init__(self, in_nodes, hidden_nodes, out_nodes, 
                learning_rate, activation_function = sigmoid):
        #learning rate alpha
        self.learning_rate = learning_rate

        # This is a 3-column neural net.

        #n, the number of features for the input vectors
        self.in_nodes = in_nodes
        
        # This is sort of arbitrary
        self.hidden_nodes = hidden_nodes
        
        #The number of classifications
        self.out_nodes = out_nodes

        # Weight matrices.
        # Weights are w_i_j, where the weight is the edge weight from
        # node i in layer l to node j in layer l+1.
        # Weights are from (-1/sqr(number of nodes), 1/sqr(number of nodes))

        # h x n matrix
        self.wih = random.normal(0.0, self.in_nodes ** -0.5, 
                                    (self.hidden_nodes, self.in_nodes))
        # out x h matrix
        self.who = random.normal(0.0, self.in_nodes ** -0.5, 
                                    (self.out_nodes, self.hidden_nodes))

        self.activation_function = activation_function

    def train(self, inputs_list, targets_list):

        #####################
        # Forward propagation
        #####################
        # inputs_list is the list of input vectors for training data.
        # targets_list is the list of the y values for those training data 
        #   (that is, the correct labels)
        # (i.e., actual classifications)
        inputs = array(inputs_list, ndmin = 2)
        targets = array(targets_list, ndmin = 2)
        
        # Signals into hidden layer
        # w_ih x input^T = h x n * n x m = h x m
        hidden_inputs = dot(self.wih, inputs.T)
        # Signal out of hidden layer
        hidden_outputs = self.activation_function(hidden_inputs)

        # Signal into output layer
        # who x hidden_out = out x h * h x m
        final_inputs = dot(self.who, hidden_outputs)
        final_outputs = self.activation_function(final_inputs)

        #################
        # backpropagation
        #################
        output_errors = targets.T - final_outputs

        # Apply the weights on the edges from the hidden layer
        # to the output later to the error values
        # h x out * out x m = h x m
        hidden_errors = dot(self.who.T, output_errors)

        # Update the weights for the hidden->output edges
        h_to_o_error = output_errors * final_outputs * (1.0 - final_outputs)

        #print(hidden_errors.shape, output_errors.shape, self.who.shape, h_to_o_error.shape)

        self.who += self.learning_rate * dot(h_to_o_error, transpose(hidden_outputs))

        # Update the weights for the input->hidden edges
        i_to_h_error = hidden_errors * hidden_outputs * (1.0 - hidden_outputs)
        self.wih += self.learning_rate * dot(i_to_h_error, inputs)
    
    # Forward propagation function
    def query(self, input_list):
        #Convert the inputs to a 2D array
        inputs = array(input_list, ndmin = 2)

        #Weights applied to each input (transposed from row vectors)
        #X_hidden = W_input->hidden . (Inputs)^T
        # h x n * n x m (number of examples) = h x m
        hidden_inputs = dot(self.wih, inputs.T)

        #The signal emerging from the hidden layer
        hidden_outputs = self.activation_function(hidden_inputs)

        #X_output = W_hidden->output . O_hidden
        # out x h * h x m
        final_inputs = dot(self.who, hidden_outputs)

        #Signal emerging from the final output layer
        # out x m --- set of output nodes for each example
        return self.activation_function(final_inputs)

    def normalizeValues(self, values_list, max_value, mult_epison, add_epsilon):
        return (asfarray(values_list) / max_value * mult_epsilon) + add_epsilon

    def train_network(self, training_set, training_labels):
        '''
        Runs train on a list of data and a list of its labels until exhausted.
        training_set is a numpy array of vectors of normalized training data
        training_labels is a numpy array of their labels
        '''
        if training_set.size == 0:
            return
        elif len(training_set) != len(training_labels):
            print("Error: Training set size ", len(training_set),\
                "and labels set size", len(training_labels), "do not match")
            return
        else:
            for i in range(len(training_set)):
                targets = zeros(self.out_nodes) + 0.01
                targets[training_labels[i]] = 0.99
                self.train(training_set[i], targets)

    def test(self, test_set, test_labels):
        '''
        Queries a list of data and a list of its labels and keeps track of
        which the network gets correct.
        training_set is an array of vectors of normalized training data
        training_labels is an array of their labels
        '''
        if test_set.size == 0:
            return
        elif len(test_set) != len(test_labels):
            print("Error: Test set size ", len(test_set),\
                "and labels set size", len(test_labels), "do not match")
            return

        scorecard = []
        for i in range(len(test_set)):
            outputs = self.query(test_set[i])
            # Numpy's argmax gets the index of the largest value in a vector
            label = argmax(outputs)
            correct_label = test_labels[i]
            #print("Correct label: ", correct_label, "; network output: ", label)
            
            if label == correct_label:
                scorecard.append(1)
            else:
                scorecard.append(0)
        self.scorecard = scorecard
