import matplotlib.pyplot as plt

def pause():
    #################################################################################
    ''' Request arbitrary user input before continuing.'''
    #################################################################################
    input("Press ENTER to continue.")

rates = {0.08 : .9, 0.1 : .91, 0.12 : .94, .14 : .945,
             .16 : .95, .18 : .96, .2 : .97, .22 : .96, 
             .24 : .95, .26 : .94, .28 : .94, .3 : .92}

names = list(rates.keys())
values = list(rates.values())

fig, ax = plt.subplots()
ax.plot(names, values, label = 'rate')
fig.suptitle('Accuracy results with different learning rates')
plt.show()
pause()
